package fasar.airq;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicReference;

public class CsvLineReader {

    ByteBuffered byteBuffered;
    AtomicReference date = new AtomicReference(LocalDate.now());

    public CsvLineReader() {
        this.byteBuffered = new ByteBuffered(1024, (bytes -> {
            LocalDate currentDate = LocalDate.now();
            if (!date.compareAndSet(currentDate, currentDate)) {

            }
        }));
    }

    public void onLine(String line) {

    }

    private String fileName(LocalDate date) {
        String s = date.toString() + ".csv";
        return s;
    }

}

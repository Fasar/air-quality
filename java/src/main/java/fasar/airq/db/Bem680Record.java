package fasar.airq.db;

import java.util.Objects;

public class Bem680Record {
    private String source;
    private long timestamp;
    private double iaq;
    private int iaq_accuracy;
    private double temperature;
    private double humidity;
    private double pressure;
    private double raw_temperature;
    private double raw_humidity;
    private double gas;

    public Bem680Record(
            String source, long timestamp, double iaq, int iaq_accuracy, double temperature, double humidity,
            double pressure, double raw_temperature, double raw_humidity, double gas
    ) {
        this.source = source;
        this.timestamp = timestamp;
        this.iaq = iaq;
        this.iaq_accuracy = iaq_accuracy;
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        this.raw_temperature = raw_temperature;
        this.raw_humidity = raw_humidity;
        this.gas = gas;
    }

    public String getSource() {
        return source;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public double getIaq() {
        return iaq;
    }

    public int getIaq_accuracy() {
        return iaq_accuracy;
    }

    public double getTemperature() {
        return temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getPressure() {
        return pressure;
    }

    public double getRaw_temperature() {
        return raw_temperature;
    }

    public double getRaw_humidity() {
        return raw_humidity;
    }

    public double getGas() {
        return gas;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bem680Record that = (Bem680Record) o;
        return timestamp == that.timestamp &&
                Double.compare(that.iaq, iaq) == 0 &&
                iaq_accuracy == that.iaq_accuracy &&
                Double.compare(that.temperature, temperature) == 0 &&
                Double.compare(that.humidity, humidity) == 0 &&
                Double.compare(that.pressure, pressure) == 0 &&
                Double.compare(that.raw_temperature, raw_temperature) == 0 &&
                Double.compare(that.raw_humidity, raw_humidity) == 0 &&
                Double.compare(that.gas, gas) == 0 &&
                Objects.equals(source, that.source);
    }

    @Override
    public int hashCode() {
        return Objects.hash(source, timestamp, iaq, iaq_accuracy, temperature, humidity, pressure, raw_temperature, raw_humidity, gas);
    }

    @Override
    public String toString() {
        return "Bem680Record{" +
                "source='" + source + '\'' +
                ", timestamp=" + timestamp +
                ", iaq=" + iaq +
                ", iaq_accuracy=" + iaq_accuracy +
                ", temperature=" + temperature +
                ", humidity=" + humidity +
                ", pressure=" + pressure +
                ", raw_temperature=" + raw_temperature +
                ", raw_humidity=" + raw_humidity +
                ", gas=" + gas +
                '}';
    }
}


package fasar.airq.composant;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "app.web")
public class WebAppProperties {
    private String window = "2m";
    private String stepWindow = "1m";

    public String getWindow() {
        return window;
    }

    public void setWindow(String window) {
        this.window = window;
    }

    public String getStepWindow() {
        return stepWindow;
    }

    public void setStepWindow(String stepWindow) {
        this.stepWindow = stepWindow;
    }
}

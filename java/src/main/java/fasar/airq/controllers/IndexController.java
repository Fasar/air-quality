package fasar.airq.controllers;

import fasar.airq.composant.WebAppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;

@Controller
public class IndexController {

    @Autowired
    WebAppProperties webAppProperties;


    @GetMapping("/")
    public String main(
            @RequestParam(required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
            @RequestParam(required = false, defaultValue = "1")  Integer nbDays,
            Model model
    ) {
        if (date == null) {
            date = LocalDate.now();
        }
        LocalDate prevDate = date.minusDays(1);
        LocalDate nextDate = date.plusDays(1);
        model.addAttribute("prevDate", prevDate);
        model.addAttribute("date",  date);
        model.addAttribute("nextDate", nextDate);
        model.addAttribute("nbDays", nbDays);
        model.addAttribute("ppt", webAppProperties);
        return "index";
    }
}

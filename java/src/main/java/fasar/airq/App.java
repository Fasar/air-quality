package fasar.airq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Application
 */
@SpringBootApplication
public class App {
    public static void main(String[] args) throws IOException {
        AtomicBoolean running = new AtomicBoolean(true);
        Thread thread = new Thread(new UdpListener(8266, running));
        thread.start();
        Runtime.getRuntime()
                .addShutdownHook(
                        new Thread(() -> {
                            running.set(false);
                        })
                );


        SpringApplication springApplication = new SpringApplication(App.class);
        springApplication.run(args);

    }
}

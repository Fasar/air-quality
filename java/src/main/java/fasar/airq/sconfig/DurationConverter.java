package fasar.airq.sconfig;

import org.springframework.core.convert.converter.Converter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.web.bind.annotation.ControllerAdvice;

import java.time.Duration;
import java.util.Objects;

@ControllerAdvice
public class DurationConverter implements Converter<String, Duration> {


    public static Duration parseDuration(String string) {
        new StringHttpMessageConverter();
        Objects.requireNonNull(string, "Duration should not be null");
        int days = 0;
        int hours = 0;
        int minutes = 0;
        int seconds = 0;
        long millis = 0;
        int curIndex = 0;
        int dayIndex = string.indexOf("d");
        if (dayIndex > 0) {
            String dayStr = string.substring(curIndex, dayIndex);
            days = Integer.parseInt(dayStr.trim());
            curIndex = dayIndex;
            curIndex++;
        }
        int hoursIndex = string.indexOf("h");
        if (hoursIndex > 0) {
            String numStr = string.substring(curIndex, hoursIndex);
            hours = Integer.parseInt(numStr.trim());
            curIndex = hoursIndex;
            curIndex++;
        }
        int minutesIndex = string.indexOf("m");
        int milliIndex = string.indexOf("ms");
        if (minutesIndex > 0 && milliIndex != minutesIndex) {
            String numStr = string.substring(curIndex, minutesIndex);
            minutes = Integer.parseInt(numStr.trim());
            curIndex = minutesIndex;
            curIndex++;
        }
        int secondsIndex = string.indexOf("s");
        if (secondsIndex > 0 && milliIndex + 1 != secondsIndex) {
            String numStr = string.substring(curIndex, secondsIndex);
            seconds = Integer.parseInt(numStr.trim());
            curIndex = secondsIndex;
            curIndex++;
        }
        if (milliIndex > 0) {
            String numStr = string.substring(curIndex, milliIndex);
            millis = Long.parseLong(numStr.trim());
            curIndex = milliIndex;
        }

        if (curIndex == 0) {
            millis = Long.parseLong(string);
        }

        return Duration.ofMillis((((((days * 24L + hours) * 60L) + minutes) * 60L) + seconds) * 1000L + millis);
    }

    @Override
    public Duration convert(String s) {
        return DurationConverter.parseDuration(s);
    }
}

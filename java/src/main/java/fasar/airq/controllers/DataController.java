package fasar.airq.controllers;

import fasar.airq.db.Bem680Converter;
import fasar.airq.db.Bem680Record;
import fasar.airq.helper.MathUtils;
import fasar.airq.helper.StreamsFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class DataController {

    private static String CSV_SEP = ";";

    @Autowired
    private Bem680Converter bem680Converter;

    public static double pow2(double a) {
        return a * a;
    }

    @GetMapping(value = "data")
    @ResponseBody()
    public String data(
            @RequestParam(name = "date", required = false)
            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate startDate,
            @RequestParam(required = false, defaultValue = "1") Integer nbDays,
            @RequestParam(required = false, defaultValue = "1m") Duration window,
            @RequestParam(required = false, defaultValue = "20s") Duration stepWindow
    ) throws IOException {
        if (startDate == null) {
            startDate = LocalDate.now();
        }
        if (nbDays >= 10) {
            throw new IllegalArgumentException("nbDays can't be superior to 10");
        }

        // Get Data
        Stream<Bem680Record> lines = Stream.empty();

        for (int i = -1; i < nbDays; i++) {
            LocalDate curDate = startDate.plusDays(i);
            Stream<Bem680Record> stream = bem680Converter.convert(curDate);
            lines = Stream.concat(lines, stream);
        }

        // Filter wrong dates if any
        long now = Instant.now().toEpochMilli();
        lines = lines.filter(e -> e.getTimestamp() <= now);

        // Init Variable for windowing
        Instant startInstant = startDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        // Windowing
        String res = StreamsFacade
                .timeWindowed(lines, startInstant,
                        window, stepWindow,
                        r -> Instant.ofEpochMilli(r.getTimestamp())
                )
                .map(e -> e == null ? null : e.size() == 0 ? null : e)
                .filter(Objects::nonNull)
                .map(this::toCsvAvg)
                .collect(Collectors.joining("\n"));

        // Init Headers
        StringBuilder sb = new StringBuilder();
        sb.append(headers());
        // Finish
        sb.append(res);
        sb.append("\n");

        return sb.toString();
    }

    private String headers() {
        return "sensor;timestamp;iaq;iaq_stdev;temperature;humidity;pressure;gas\n";
    }

    /**
     *
     * @param record
     * @return
     */
    private String toCsvAvg(List<Bem680Record> record) {
        if (record == null || record.size() == 0) {
            return "";
        }
        Bem680Record first = record.get(0);
        Bem680Record last = record.get(record.size() - 1);
        long middle = (long) ((last.getTimestamp() - first.getTimestamp()) / 2d + first.getTimestamp());
        int size = record.size();

        double[] iqa = new double[record.size()];
        double[] temp = new double[record.size()];
        double[] hum = new double[record.size()];
        double[] press = new double[record.size()];
        double[] gas = new double[record.size()];
        for (int i = 0; i < record.size(); i++) {
            Bem680Record bem680Record = record.get(i);
            iqa[i] = bem680Record.getIaq();
            temp[i] = bem680Record.getTemperature();
            hum[i] = bem680Record.getHumidity();
            press[i] = bem680Record.getPressure();
            gas[i] = bem680Record.getGas();
        }
        double iqaAvg = MathUtils.means(iqa);
        double tempAvg = MathUtils.means(temp);
        double humAvg = MathUtils.means(hum);
        double pressAvg = MathUtils.means(press);
        double gasAvg = MathUtils.means(gas);

        StringBuilder sb = new StringBuilder();
        sb.append(first.getSource());
        sb.append(CSV_SEP);
        sb.append(middle);
        sb.append(CSV_SEP);
        sb.append(iqaAvg);
        sb.append(CSV_SEP);
        sb.append(MathUtils.stddev(iqa, iqaAvg));
        sb.append(CSV_SEP);
        sb.append(tempAvg);
        sb.append(CSV_SEP);
        sb.append(humAvg);
        sb.append(CSV_SEP);
        sb.append(pressAvg);
        sb.append(CSV_SEP);
        sb.append(gasAvg);

        return sb.toString();
    }

    private double dev(Bem680Record powAvg, Bem680Record acc, Bem680Record elem, Function<Bem680Record, Double> toDouble) {
        return toDouble.apply(acc) + pow2(toDouble.apply(elem) - toDouble.apply(powAvg));
    }
}

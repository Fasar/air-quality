package fasar.airq;

import java.util.function.Consumer;

/**
 * Help to write file by sector of 512 or 1024 bytes
 */
public class ByteBuffered {

    private final int bufferSize;
    private byte[] buff;
    private int currentIndex = 0;
    private Object lock = new Object();

    private Consumer<byte[]> writer;

    public ByteBuffered(int bufferSitze, Consumer<byte[]> writer) {
        this.bufferSize = bufferSitze;
        this.buff = new byte[bufferSitze];
        this.writer = writer;
    }

    public void append(byte[] bytes) {
        synchronized (lock) {
            int offset = 0;
            int len = bytes.length;
            while (len > 0) {
                int freeByteInBuffer = bufferSize - currentIndex;
                int toCopy = Math.min(len, freeByteInBuffer);
                System.arraycopy(bytes, offset, buff, currentIndex, toCopy);
                len -= toCopy;
                currentIndex += toCopy;
                if (currentIndex == bufferSize) {
                    writer.accept(buff);
                    currentIndex = 0;
                }
            }
        }
    }

    public void flush() {
        synchronized (lock) {
            if (currentIndex == 0) {
                return;
            }
            byte[] bytes = new byte[currentIndex];
            System.arraycopy(buff, 0, bytes, 0, currentIndex);
            writer.accept(bytes);
            currentIndex = 0;
        }
    }
}


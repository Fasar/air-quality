package fasar.airq.db;

import com.codepoetics.protonpack.StreamUtils;
import fasar.airq.AppHelper;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

@Service
public class Bem680Converter {
    public static Logger LOG = LoggerFactory.getLogger(Bem680Record.class);

    public Stream<Bem680Record> convert(LocalDate date) throws IOException {
        File file = new File(AppHelper.fileName(date));
        List<String> lines = FileUtils.readLines(file, UTF_8);
        Stream<Bem680Record> bem680RecordStream = lines.stream()
                .map(this::toRecord)
                .filter(Objects::nonNull)
                ;
        if (date.isBefore(LocalDate.of(2020, 3, 19))) {
            Instant curInstant = date.atStartOfDay(ZoneId.systemDefault()).toInstant();
            int nbPoints = lines.size();
            double dstepMilli = 1000 * 24.0 * 3600 / nbPoints;
            bem680RecordStream = StreamUtils.zipWithIndex(bem680RecordStream)
                    .map(pair -> {
                        long index = pair.getIndex();
                        Bem680Record value = pair.getValue();
                        double v = index * dstepMilli;
                        long ts = value.getTimestamp() + (int) v;
                        return new Bem680Record(
                                value.getSource(), ts, value.getIaq(), value.getIaq_accuracy(), value.getTemperature(),
                                value.getHumidity(), value.getPressure(), value.getRaw_temperature(), value.getRaw_humidity(),
                                value.getGas());
                    });
        }
        return bem680RecordStream;
    }


    /**
     * timestamp;iaq;iaq_accuracy;temperature;humidity;pressure;raw_temperature;raw_humidity;gas;sensor
     *
     * @param s the line to convert
     * @return a Record Entity
     */
    private Bem680Record toRecord(String s) {
        try {
            String[] split = s.split(";");
            if (split.length != 10) {
                return null;
            }
            String source = split[9];
            long timestamp = Long.parseLong(split[0]);
            double iaq = Double.parseDouble(split[1]);
            int iaq_accuracy = Integer.parseInt(split[2]);
            double temperature = Double.parseDouble(split[3]);
            double humidity = Double.parseDouble(split[4]);
            double pressure = Double.parseDouble(split[5]);
            double raw_temperature = Double.parseDouble(split[6]);
            double raw_humidity = Double.parseDouble(split[7]);
            double gas = Double.parseDouble(split[8]);
            return new Bem680Record(source, timestamp, iaq, iaq_accuracy, temperature, humidity, pressure, raw_temperature, raw_humidity, gas);
        } catch (Exception ex) {
            LOG.error("cant't convert " + s, ex);
            return null;
        }
    }
}

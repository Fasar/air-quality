# Air-Quality

This is a system to check the air quality of rooms.
It store data with a java server that can be run in raspberry pi.
The sensor is BEM680 linked with an arduino ESP8266 to send the data.

There is a description here : https://fasar.frama.io/geekage/esp/Mesure_Qualite_Air.html

package fasar.airq;

import fasar.airq.composant.RaspiBufferedOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;


public class UdpListener implements Runnable {
    private static Logger LOG = LoggerFactory.getLogger(UdpListener.class);

    private DatagramSocket socket;
    private AtomicBoolean running;

    private PrintWriter writer = null;
    private LocalDate writerDate = null;

    public UdpListener(int port, AtomicBoolean running) throws SocketException {
        socket = new DatagramSocket(port);
        this.running = running;
    }

    @Override
    public void run() {
        try {
            runPrivate();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void runPrivate() throws IOException {
        byte[] buf = new byte[1500];

        while (running.get()) {
            DatagramPacket packet = new DatagramPacket(buf, buf.length);
            socket.receive(packet);

            LocalDate now = LocalDate.now();
            if (!now.equals(writerDate)) {
                closeCurrentWriter();
                String fileName = AppHelper.fileName(now);
                try {
                    File file = new File(fileName);
                    FileOutputStream out1 = new FileOutputStream(file, true);
                    RaspiBufferedOutputStream out = new RaspiBufferedOutputStream(out1);
                    OutputStreamWriter outWriter = new OutputStreamWriter(out, UTF_8);
                    writer = new PrintWriter(outWriter, true);
                    writerDate = now;
                } catch (Exception e) {
                    LOG.error("Can't open a new the writer on " + fileName + " : " + e.getMessage(), e);
                }
            }


            try {
                String received = new String(packet.getData(), 0, packet.getLength());
                String[] split = received.split(";");
                split[0] = "" + System.currentTimeMillis();
                split[split.length - 1] = split[split.length - 1].replaceAll("\n", "");
                received = Arrays.stream(split).collect(Collectors.joining(";"));
                received += ";" + packet.getAddress().getHostAddress() ;
                LOG.debug("Packet: {}", received);
                if(writer != null){
                    writer.println(received);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        closeCurrentWriter();
        socket.close();
    }

    private void closeCurrentWriter() {
        if (writer != null) {
            try {
                writer.close();
            } catch (Exception e) {
                LOG.error("Can't close the writer: " + e.getMessage(), e);
            }
        }
    }

}

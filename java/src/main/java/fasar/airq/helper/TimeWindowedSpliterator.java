package fasar.airq.helper;

import java.time.Duration;
import java.time.Instant;
import java.util.LinkedList;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.function.Function;

public class TimeWindowedSpliterator<T> implements Spliterator<List<T>> {

    private final Spliterator<T> source;
    private final Function<T, Instant> convertToInstant;
    private final Duration stepWindow;
    LinkedList<T> next = new LinkedList<>();
    private Instant currentWindowInstantStart;
    private Instant currentWindowInstantEnd;
    private T last;
    private boolean windowSeeded;


    public TimeWindowedSpliterator(Spliterator<T> input, Instant start, Duration window, Duration stepWindow, Function<T, Instant> convertToInstant) {
        this.currentWindowInstantStart = start;
        this.convertToInstant = convertToInstant;
        currentWindowInstantEnd = start.plus(window);
        this.stepWindow = stepWindow;
        source = input;
    }

    public void advanceOneStep() {
        this.currentWindowInstantStart = this.currentWindowInstantStart.plus(stepWindow);
        this.currentWindowInstantEnd = this.currentWindowInstantEnd.plus(stepWindow);
    }

    protected boolean isElemInWindow(T elem) {
        final Instant instant = convertToInstant.apply(elem);
        return !instant.isBefore(currentWindowInstantStart) && instant.isBefore(currentWindowInstantEnd);
    }


    private boolean hasNext() {
        if (!windowSeeded) {
            seedWindow();
            windowSeeded = true;
        } else {
            if (this.last == null) {
                return false;
            }
            advanceOneStep();
            filterBufferWithDataInWindow();
            nextWindow();
        }
        return true;
    }

    private void seedWindow() {
        while (source.tryAdvance(next::add)) {
            if (next.size() == 0) {
                break;
            }
            final T elem = next.getLast();
            // Remove if element is before currentWindowInstantStart
            final Instant instant = this.convertToInstant.apply(elem);
            if (instant.isBefore(this.currentWindowInstantStart)) {
                next.removeLast();
            }
            // Remove if element is after or equals currentWindowInstantEnd.
            // But keep it in a buffer.
            if (!instant.isBefore(currentWindowInstantEnd)) {
                this.last = next.removeLast();
                break;
            }
        }
    }

    private void filterBufferWithDataInWindow() {
        // Remove head of buffer not in rage
        T curElem = (next.isEmpty()) ? null : next.getFirst();
        while (curElem != null && !isElemInWindow(curElem)) {
            next.removeFirst();
            curElem = (next.isEmpty()) ? null : next.getFirst();
        }
    }

    private void nextWindow() {
        // Add the last if in range
        if (isElemInWindow(this.last)) {
            next.add(this.last);
            this.last = null;
            seedWindow();
        } else {
            Instant curInstant = this.convertToInstant.apply(this.last);
            if (curInstant.isBefore(this.currentWindowInstantStart)) {
                this.last = null;
                seedWindow();
            }
        }
    }

    @Override
    public boolean tryAdvance(Consumer<? super List<T>> action) {
        if (hasNext()) {
            LinkedList<T> queue = new LinkedList<>(next);
            action.accept(queue);
            return true;
        }
        return false;
    }

    @Override
    public Spliterator<List<T>> trySplit() {
        return null;
    }

    @Override
    public long estimateSize() {
        return 0;
    }

    @Override
    public int characteristics() {
        return source.characteristics() & ~(Spliterator.SIZED | Spliterator.ORDERED);
    }

}

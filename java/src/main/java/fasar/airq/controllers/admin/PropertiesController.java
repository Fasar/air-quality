package fasar.airq.controllers.admin;

import fasar.airq.composant.WebAppProperties;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PropertiesController {

    @Autowired
    WebAppProperties webAppProperties;

    @GetMapping("/admin/web-properties")
    public String ppt(Model model) throws InvocationTargetException, IllegalAccessException {
        Class<? extends WebAppProperties> aClass = webAppProperties.getClass();
        Method[] methods = aClass.getMethods();
        List<Pair<String, String>> pairs = new ArrayList<>();
        for (Method method : methods) {
            String methodName = method.getName();
            if (methodName.startsWith("get") && ! methodName.equals("getClass")) {
                String attributName = methodName.substring(3);
                Object objRet = method.invoke(webAppProperties);
                String attributValue = objRet==null?null:objRet.toString();
                pairs.add(Pair.of(attributName, attributValue));
            }
        }

        model.addAttribute("list", pairs);
        return "admin/web-properties";
    }

    @PostMapping("/admin/web-properties")
    public String postPpt(
            @RequestParam String key,
            @RequestParam String value,
            Model model
    ) throws InvocationTargetException, IllegalAccessException {
        Class<? extends WebAppProperties> aClass = webAppProperties.getClass();
        Method[] methods = aClass.getMethods();
        for (Method method : methods) {
            String methodName = method.getName();
            if (methodName.startsWith("set") ) {
                String attributName = methodName.substring(3);
                if (attributName.equals(key)) {
                    method.invoke(webAppProperties, value);
                }
            }
        }

        return ppt(model);
    }

}

package fasar.airq.controllers;

import fasar.airq.sconfig.DurationConverter;
import org.junit.jupiter.api.Test;

import java.time.Duration;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DurationConverterTest {
    @Test
    public void testParseDuration() {
        assertEquals(Duration.ofDays(1), DurationConverter.parseDuration("1d"));
        assertEquals(Duration.ofDays(1), DurationConverter.parseDuration("1 d"));
        assertEquals(Duration.ofDays(1), DurationConverter.parseDuration(" 1 d"));
        assertEquals(Duration.ofHours(1), DurationConverter.parseDuration(" 1 h"));
        assertEquals(Duration.ofHours(1), DurationConverter.parseDuration(" 1h"));
        assertEquals(Duration.ofHours(1), DurationConverter.parseDuration("1h"));
        assertEquals(Duration.ofMinutes(1), DurationConverter.parseDuration("1m"));
        assertEquals(Duration.ofSeconds(1), DurationConverter.parseDuration("1s"));
        assertEquals(Duration.ofMillis(1), DurationConverter.parseDuration("1ms"));
        assertEquals(Duration.ofMillis(1), DurationConverter.parseDuration("1"));
        assertEquals(Duration.ofDays(1).plusHours(1).plusSeconds(1), DurationConverter.parseDuration("1d1h1s"));
        assertEquals(Duration.ofDays(1).plusHours(1).plusMinutes(1), DurationConverter.parseDuration("1d1h1m"));
        assertEquals(Duration.ofDays(1).plusHours(1).plusMillis(1), DurationConverter.parseDuration("1d1h1ms"));
        assertEquals(Duration.ofHours(1).plusMillis(1), DurationConverter.parseDuration("1 h 1 ms"));

    }


}
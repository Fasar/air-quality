package fasar.airq.composant;

import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * OutputStream compatible with SD card to optimize the write on the card.
 * <p>
 * It guaranty to write blocks of 512 bytes.
 * <p>
 * It optimize the write process by guaranty the output is done with at minimal bufferSize bytes
 */
public class RaspiBufferedOutputStream extends FilterOutputStream {

    protected byte[] buff;
    protected int count;
    protected int blockSize;
    byte[] tmpBuff;

    /**
     * @param out the output stream to wrap
     * @param bufferSize set the minimal size you want the write is
     * @param blockSize  set the number of byte to write together
     */
    public RaspiBufferedOutputStream(OutputStream out, int bufferSize, int blockSize) {
        super(out);
        if (bufferSize < blockSize) {
            throw new IllegalArgumentException("Buffer Size can't be less than the blockSize");
        }
        this.count = 0;
        buff = new byte[bufferSize];
        tmpBuff = new byte[blockSize];
        this.blockSize = blockSize;
    }

    public RaspiBufferedOutputStream(OutputStream out) {
        this(out, 8192, 512);
    }

    public int getCount() {
        return count;
    }

    private void flushInternalBuffer() throws IOException {
        int nbWritten = writeByBlockSize(buff, 0, count);
        if (nbWritten > 0) {
            count = count - nbWritten;
            System.arraycopy(buff, nbWritten, buff, 0, count);
        }
    }

    private void fullyFlushInternalBuffer() throws IOException {
        out.write(buff, 0, count);
        count = 0;
    }

    /**
     * Write the buffer from offset with max of nbToWrite element on the output.
     * It guaranty to write a modulo blockSize element.
     * <p>
     * It returns the number of elements written
     *
     * @param buffer the buffer th write
     * @param offset the offset of the buffer
     * @param nbToWrite the nb of elements asked to write
     * @return the number of elements written
     * @throws IOException if fail to write on outputStream
     */
    private int writeByBlockSize(byte[] buffer, int offset, int nbToWrite) throws IOException {
        int nbToFlush = nbToWrite / blockSize;
        if (nbToFlush > 0) {
            int toWrite = nbToFlush * blockSize;
            out.write(buffer, offset, toWrite);
            return toWrite;
        }
        return 0;
    }

    /**
     * The write method append the byte in the buffer.
     *
     * @param b the byte to write
     * @throws IOException if fail to write on outputStream
     */
    @Override
    public void write(int b) throws IOException {
        if (count >= buff.length) {
            flushInternalBuffer();
        }
        buff[count++] = (byte) b;
    }

    @Override
    public void write(byte[] b) throws IOException {
        write(b, 0, b.length);
    }

    @Override
    public void write(byte[] b, int off, int len) throws IOException {
        if (count + len >= buff.length) {
            flushInternalBuffer();
            int newLen;
            int newOffset;
            if (count > 0) {
                // Create a tmp buffer of 512 bytes in tmpBuff
                // copy the current elements in the buffer in the tmp buffer
                // then copy the firsts elements of the buffer to write int the
                // tmp buffer, then write the tmpbuffer on outputStream.
                System.arraycopy(buff, 0, tmpBuff, 0, count);
                newLen = this.blockSize - count;
                System.arraycopy(buff, count, tmpBuff, off, newLen);
                writeByBlockSize(tmpBuff, 0, blockSize);
                newOffset = off + newLen;
                newLen = len - newLen;
            } else {
                newLen = len;
                newOffset = off;
            }
            int nbWritten = writeByBlockSize(b, newOffset, newLen);
            newOffset = newOffset + nbWritten;
            newLen = len - nbWritten;
            // Copy the rest of elements not written on outputStream in the buffer
            System.arraycopy(b, newOffset, buff, 0, newLen);
            count = newLen;
        } else {
            System.arraycopy(b, off, buff, count, len);
            count = count + len;
        }
    }

    @Override
    public void flush() throws IOException {
        flushInternalBuffer();
    }

    @Override
    public void close() throws IOException {
        fullyFlushInternalBuffer();
        super.close();
    }
}

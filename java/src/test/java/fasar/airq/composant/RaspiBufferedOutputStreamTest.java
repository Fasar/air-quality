package fasar.airq.composant;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.OutputStream;

@ExtendWith(MockitoExtension.class)
class RaspiBufferedOutputStreamTest {

    @Mock
    OutputStream out;

    @Test
    void givenAnEmptStream_whenUseWrite_thenItFlushWhenBufferFull() throws IOException {
        RaspiBufferedOutputStream stream = new RaspiBufferedOutputStream(out, 200, 100);

        for (int i = 0; i < 200; i++) {
            stream.write(66);
        }
        Mockito.verify(out, Mockito.times(0)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        stream.write(66);
        Mockito.verify(out, Mockito.times(1)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        stream.close();
        Mockito.verify(out, Mockito.times(2)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void givenStreamWithData_whenClose_thenItFlushTheBuffer() throws IOException {
        RaspiBufferedOutputStream stream = new RaspiBufferedOutputStream(out, 200, 100);
        for (int i = 0; i < 15; i++) {
            stream.write(66);
        }
        Mockito.verify(out, Mockito.times(0)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        stream.close();
        Mockito.verify(out, Mockito.times(1)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
    }



    @Test
    void givenStreamW_whenFiveTimeBufferSize_thenItWriteData() throws IOException {
        RaspiBufferedOutputStream stream = new RaspiBufferedOutputStream(out, 200, 100);
        for (int i = 0; i < 100; i++) {
            stream.write(66);
        }
        Mockito.verify(out, Mockito.times(0)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        byte[] buffer = new byte[400];
        for (int i = 0; i < 400; i++) {
            buffer[i] = 33;
        }
        stream.write(buffer);
        Mockito.verify(out, Mockito.times(2)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        stream.close();
        Mockito.verify(out, Mockito.times(3)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
    }


    @Test
    void givenStreamW_whenWriteMoreThanBufferSize_thenItWriteData() throws IOException {
        RaspiBufferedOutputStream stream = new RaspiBufferedOutputStream(out, 200, 100);
        for (int i = 0; i < 55; i++) {
            stream.write(66);
        }
        Mockito.verify(out, Mockito.times(0)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        byte[] buffer = new byte[400];
        for (int i = 0; i < 400; i++) {
            buffer[i] = 33;
        }
        stream.write(buffer);
        Mockito.verify(out, Mockito.times(2)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        stream.close();
        Mockito.verify(out, Mockito.times(3)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
    }


    @Test
    void givenEmptyStream_whenWriteBufferSizeOrModulo_thenItWriteDataOnlyOnce() throws IOException {
        RaspiBufferedOutputStream stream = new RaspiBufferedOutputStream(out, 200, 100);
        Mockito.verify(out, Mockito.times(0)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        byte[] buffer = new byte[400];
        for (int i = 0; i < 400; i++) {
            buffer[i] = 33;
        }
        stream.write(buffer);
        Mockito.verify(out, Mockito.times(1)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        stream.close();
        Mockito.verify(out, Mockito.times(2)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void givenAnEmptyStrean_whenWrtieBufferSizePlus50_then50ShoudStayInBuffer() throws IOException {
        RaspiBufferedOutputStream stream = new RaspiBufferedOutputStream(out, 200, 100);
        Mockito.verify(out, Mockito.times(0)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        byte[] buffer = new byte[100];
        for (int i = 0; i < 100; i++) {
            buffer[i] = 33;
        }

        // Fill 150 elements
        for (int i = 0; i < 3; i++) {
            stream.write(buffer, 0, 50);
            Mockito.verify(out, Mockito.times(0)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        }

        stream.write(buffer);
        Mockito.verify(out, Mockito.times(2)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());
        stream.close();
        Mockito.verify(out, Mockito.times(3)).write(Mockito.any(), Mockito.anyInt(), Mockito.anyInt());

    }
}
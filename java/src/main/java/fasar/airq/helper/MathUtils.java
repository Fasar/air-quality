package fasar.airq.helper;

public class MathUtils {

    public static double means(double[] values) {
        if (values == null) {
            throw new IllegalArgumentException("null values is not acceptable");
        }
        if (values.length == 0) {
            return Double.NaN;
        }
        double sum = Double.NaN;
        sum = 0.0;
        for (int i = 0; i < values.length; i++) {
            sum += values[i];
        }
        return sum / values.length;
    }

    public static double stddev(double[] values, double mean) {
        if (values == null) {
            throw new IllegalArgumentException("null values is not acceptable");
        }
        double var;
        if (values.length == 0) {
            var = Double.NaN;
        } else if (values.length == 1) {
            var = 0d;
        } else {
            double accum = 0.0;
            double dev = 0.0;
            for (int i = 0; i < values.length; i++) {
                dev = values[i] - mean;
                accum += dev * dev;
            }
            accum = accum / values.length;
            var = Math.sqrt(accum);
        }
        return var;
    }
}

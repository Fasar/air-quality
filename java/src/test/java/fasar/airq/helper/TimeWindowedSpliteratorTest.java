package fasar.airq.helper;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Spliterator;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;

public class TimeWindowedSpliteratorTest {

    Stream<Pair<Instant, Integer>> generate(Instant start, Duration step) {
        return StreamSupport.stream(new Spliterator<Pair<Instant, Integer>>() {
            Instant curInstant = start;
            Integer curNumber = 0;

            @Override
            public boolean tryAdvance(Consumer<? super Pair<Instant, Integer>> consumer) {
                consumer.accept(Pair.of(curInstant, curNumber++));
                curInstant = curInstant.plus(step);
                return true;
            }

            @Override
            public Spliterator<Pair<Instant, Integer>> trySplit() {
                return null;
            }

            @Override
            public long estimateSize() {
                return 0;
            }

            @Override
            public int characteristics() {
                return 0;
            }
        }, false);
    }


    @Test
    public void simple_case() {
        final Instant start = Instant.parse("2017-01-01T00:00:00Z");
        Stream<Pair<Instant, Integer>> integerStream = generate(start, Duration.ofSeconds(1)).limit(10);
        final List<Pair<Instant, Integer>> collect1 = integerStream.collect(Collectors.toList());

        final List<List<Pair<Instant, Integer>>> collect = StreamsFacade.timeWindowed(
                collect1.stream(), start, Duration.ofSeconds(5), Duration.ofSeconds(5), Pair::getKey)
                .collect(toList());

        List<List<Integer>> simple = collect
                .stream()
                .map(e -> e.stream().map(Pair::getValue)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

        assertThat(simple, contains(
                asList(0, 1, 2, 3, 4),
                asList(5, 6, 7, 8, 9)
        ));

    }


    @Test
    public void windowing_on_limited_list() {
        final Instant start = Instant.parse("2017-01-01T00:00:00Z");
        Stream<Pair<Instant, Integer>> integerStream = generate(start, Duration.ofSeconds(1)).limit(8);
        final List<Pair<Instant, Integer>> collect1 = integerStream.collect(Collectors.toList());

        final List<List<Pair<Instant, Integer>>> collect = StreamsFacade.timeWindowed(
                collect1.stream(), start, Duration.ofSeconds(5), Duration.ofSeconds(2), Pair::getKey)
                .collect(toList());

        List<List<Integer>> simple = collect
                .stream()
                .map(e -> e.stream().map(Pair::getValue)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

        assertThat(simple, contains(
                asList(0, 1, 2, 3, 4),
                asList(2, 3, 4, 5, 6),
                asList(4, 5, 6, 7)
        ));

    }

    @Test
    public void windowing_start_after_stream_list() {
        final Instant start = Instant.parse("2017-01-01T00:00:00Z");
        Stream<Pair<Instant, Integer>> integerStream = generate(start, Duration.ofSeconds(1)).limit(18);
        final List<Pair<Instant, Integer>> collect1 = integerStream.collect(Collectors.toList());

        final List<List<Pair<Instant, Integer>>> collect = StreamsFacade.timeWindowed(
                collect1.stream(), start.plusSeconds(10), Duration.ofSeconds(5), Duration.ofSeconds(2), Pair::getKey)
                .collect(toList());

        List<List<Integer>> simple = collect
                .stream()
                .map(e -> e.stream().map(Pair::getValue)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

        assertThat(simple, contains(
                asList(10, 11, 12, 13, 14),
                asList(12, 13, 14, 15, 16),
                asList(14, 15, 16, 17)
        ));

    }


    @Test
    public void windowing_start_before_stream_list() {
        final Instant start = Instant.parse("2017-01-01T00:00:00Z");
        Stream<Pair<Instant, Integer>> integerStream = generate(start, Duration.ofSeconds(1)).limit(8);
        final List<Pair<Instant, Integer>> collect1 = integerStream.collect(Collectors.toList());

        final List<List<Pair<Instant, Integer>>> collect = StreamsFacade.timeWindowed(
                collect1.stream(), start.minusSeconds(10), Duration.ofSeconds(5), Duration.ofSeconds(2), Pair::getKey)
                .collect(toList());

        List<List<Integer>> simple = collect
                .stream()
                .map(e -> e.stream().map(Pair::getValue)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

        assertThat(simple, contains(
                asList(), asList(), asList(),
                asList(0),
                asList(0, 1, 2),
                asList(0, 1, 2, 3, 4),
                asList(2, 3, 4, 5, 6),
                asList(4, 5, 6, 7)
        ));

    }


    @Test
    public void windowing_stream_hole_inside_list() {
        final Instant start = Instant.parse("2017-01-01T00:00:00Z");
        Instant holeStart = start.plusSeconds(3);
        Instant holeEnd = start.plusSeconds(10);

        Stream<Pair<Instant, Integer>> integerStream =
                generate(start, Duration.ofSeconds(1))
                        .limit(16)
                        .filter(e -> (e.getKey().isBefore(holeStart)) || (e.getKey().isAfter(holeEnd)));
        final List<Pair<Instant, Integer>> collect1 = integerStream.collect(Collectors.toList());

        final List<List<Pair<Instant, Integer>>> collect = StreamsFacade.timeWindowed(
                collect1.stream(), start, Duration.ofSeconds(5), Duration.ofSeconds(2), Pair::getKey)
                .collect(toList());

        List<List<Integer>> simple = collect
                .stream()
                .map(e -> e.stream().map(Pair::getValue)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

        assertThat(simple, contains(
                asList(0, 1, 2),
                asList(2),
                asList(),
                asList(),
                asList(11, 12),
                asList(11, 12, 13, 14),
                asList(12, 13, 14, 15)
        ));

    }


    @Test
    public void windowing_stream_big_step() {
        final Instant start = Instant.parse("2017-01-01T00:00:00Z");
        Instant holeStart = start.plusSeconds(3);
        Instant holeEnd = start.plusSeconds(10);

        Stream<Pair<Instant, Integer>> integerStream =
                generate(start, Duration.ofSeconds(1))
                        .limit(16);
        final List<Pair<Instant, Integer>> collect1 = integerStream.collect(Collectors.toList());

        final List<List<Pair<Instant, Integer>>> collect = StreamsFacade.timeWindowed(
                collect1.stream(), start, Duration.ofSeconds(5), Duration.ofSeconds(10), Pair::getKey)
                .collect(toList());

        List<List<Integer>> simple = collect
                .stream()
                .map(e -> e.stream().map(Pair::getValue)
                        .collect(Collectors.toList()))
                .collect(Collectors.toList());

        assertThat(simple, contains(
                asList(0, 1, 2, 3, 4),
                asList(10, 11, 12, 13, 14),
                asList()
        ));

    }


//
//    @Test
//    public void
//    windowing_on_list_two_overlap() {
//        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
//
//        List<List<Integer>> windows = StreamsFacade.windowed(integerStream, 3, 2).collect(toList());
//
//        assertThat(windows, contains(
//                asList(1, 2, 3),
//                asList(3, 4, 5)));
//    }
//
//    @Test
//    public void
//    windowing_on__empty_list() {
//        ArrayList<Integer> ints = new ArrayList<>();
//
//        ints.stream().collect(maxBy((a, b) -> a.toString().compareTo(b.toString())));
//
//        List<List<Integer>> windows = StreamsFacade.windowed(ints.stream(), 2).collect(toList());
//
//        assertThat(windows, iterableWithSize(0));
//    }
//
//    @Test
//    public void
//    windowing_on_list_two_overlap_allow_lesser_size() {
//        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
//
//        List<List<Integer>> windows = StreamsFacade.windowed(integerStream, 2, 2, true).collect(toList());
//
//        assertThat(windows, contains(
//                asList(1, 2),
//                asList(3, 4),
//                asList(5)));
//    }
//
//    @Test
//    public void
//    windowing_on_list_one_overlap_allow_lesser_size_multiple_lesser_windows() {
//        Stream<Integer> integerStream = Stream.of(1, 2, 3, 4, 5);
//
//        List<List<Integer>> windows = StreamsFacade.windowed(integerStream, 3, 1, true).collect(toList());
//
//        assertThat(windows, contains(
//                asList(1, 2, 3),
//                asList(2, 3, 4),
//                asList(3, 4, 5),
//                asList(4, 5),
//                asList(5)));
//    }
//

}
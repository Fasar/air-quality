package fasar.airq.helper;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class StreamsFacade {
    public static <T> Stream<List<T>> timeWindowed(
            Stream<T> source, Instant start,
            Duration window, Duration stepWindow,
            Function<T, Instant> convertToInstant
    ) {
        return StreamSupport.stream(new TimeWindowedSpliterator<>(
                source.spliterator(), start, window, stepWindow, convertToInstant
        ), false);
    }
}

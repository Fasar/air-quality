package fasar.airq;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.concurrent.atomic.AtomicReference;

import static java.lang.Integer.valueOf;

public class ByteBufferedTest {

    @Test
    public void testWrite() {
        AtomicReference<Integer> res = new AtomicReference<>(0);
        ByteBuffered byteBuffered = new ByteBuffered(512, (bytes -> res.getAndUpdate(a -> a + bytes.length)));

        byte[] bytes = "une sourie verte".getBytes();
        byteBuffered.append(bytes);
        Assertions.assertEquals(valueOf(0), res.get());
    }

    @Test
    public void testWrite1() {
        AtomicReference<Integer> res = new AtomicReference<>(0);
        ByteBuffered byteBuffered = new ByteBuffered(512, (bytes -> res.getAndUpdate(a -> a + bytes.length)));

        byte[] bytes = new byte[512];
        for (int i = 0; i < 512; i++) {
            bytes[i] = 2;
        }
        byteBuffered.append(bytes);
        Assertions.assertEquals(valueOf(512), res.get());

        byteBuffered.append(bytes);
        Assertions.assertEquals(valueOf(1024), res.get());

        byteBuffered.append("a".getBytes());
        Assertions.assertEquals(valueOf(1024), res.get());
        byteBuffered.flush();
        Assertions.assertEquals(valueOf(1025), res.get());

        byteBuffered.flush();
        Assertions.assertEquals(valueOf(1025), res.get());
    }

    @Test
    public void test3() {
        AtomicReference<Integer> res = new AtomicReference<>(0);
        ByteBuffered byteBuffered = new ByteBuffered(512, (bytes -> res.getAndUpdate(a -> a + bytes.length)));

        byte[] bytes = new byte[300];
        for (int i = 0; i < 300; i++) {
            bytes[i] = 1;
        }

        byteBuffered.append(bytes); // 300
        Assertions.assertEquals(valueOf(0), res.get());
        byteBuffered.append(bytes); // 600
        Assertions.assertEquals(valueOf(512), res.get());
        byteBuffered.append(bytes); // 900
        Assertions.assertEquals(valueOf(512), res.get());
        byteBuffered.append(bytes); // 1200
        Assertions.assertEquals(valueOf(1024), res.get());
    }
}